from django.conf.urls import url
from django.urls import include

from apps.api.cash_flow.urls import cash_flow_urlpatterns
from apps.api.ping.urls import ping_urlpatterns

urlpatterns = [
    url('', include(ping_urlpatterns)),
    url('', include(cash_flow_urlpatterns)),
]
