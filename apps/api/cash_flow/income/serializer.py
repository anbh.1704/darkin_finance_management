from rest_framework import serializers

from apps.models import Income


class IncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Income
        fields = ('id', 'name', 'type', 'created', 'modified')

class IncomeListSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    class Meta:
        model = Income
        fields = ('id', 'name', 'type', 'created', 'modified')

    def get_type(self, obj):
        return {
            'id': obj.type,
            'name': Income.MAP_TYPE_DICT[obj.type],
        }