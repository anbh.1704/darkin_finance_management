from django_filters import FilterSet, CharFilter, NumberFilter

from apps.core.filters import FilterSetMixin


class IncomeFilter(FilterSetMixin, FilterSet):
    name = CharFilter(lookup_expr='icontains')
    type = NumberFilter()
