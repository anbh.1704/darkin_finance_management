from django_filters import FilterSet, CharFilter, NumberFilter

from apps.core.filters import FilterSetMixin


class OutcomeFilter(FilterSetMixin, FilterSet):
    name = CharFilter(lookup_expr='icontains')
    type = NumberFilter()
