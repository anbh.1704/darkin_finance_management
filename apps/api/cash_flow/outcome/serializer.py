from rest_framework import serializers

from apps.models import Outcome


class OutcomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Outcome
        fields = ('id', 'name', 'type', 'created', 'modified')


class OutcomeListSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    class Meta:
        model = Outcome
        fields = ('id', 'name', 'type', 'created', 'modified')

    def get_type(self, obj):
        return {
            'id': obj.type,
            'name': Outcome.MAP_TYPE_DICT[obj.type]
        }
