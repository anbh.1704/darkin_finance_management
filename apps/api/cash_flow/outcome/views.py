from django_filters.rest_framework import DjangoFilterBackend

from apps.api.cash_flow.outcome.filters import OutcomeFilter
from apps.api.cash_flow.outcome.serializer import OutcomeSerializer, OutcomeListSerializer
from apps.core.base_model_view_set import BaseModelViewSet
from apps.repositories.outcome_repo import OutcomeRepo


class OutcomeApiView(BaseModelViewSet):
    serializer_class = OutcomeSerializer
    serializer_action_classes = {
        'list': OutcomeListSerializer,
    }
    repository_class = OutcomeRepo
    filter_backends = [DjangoFilterBackend]
    filterset_class = OutcomeFilter
