from rest_framework import routers

from apps.api.cash_flow.income.views import IncomeApiView
from apps.api.cash_flow.outcome.views import OutcomeApiView

cash_flow_router = routers.DefaultRouter()
cash_flow_router.register("cash-flow/income", IncomeApiView, basename="cash-flow")
cash_flow_router.register("cash-flow/outcome", OutcomeApiView, basename="cash-flow")
cash_flow_urlpatterns = cash_flow_router.urls
