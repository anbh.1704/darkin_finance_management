from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.repositories.income_outcome_statistics_repo import IncomeOutcomeStatisticRepo
from apps.repositories.income_repo import IncomeRepo


@api_view(['GET'])
def ping(request):
    print(IncomeOutcomeStatisticRepo.get_by_id(1))
    return Response('pong')


