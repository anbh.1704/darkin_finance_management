from django_filters.rest_framework import DjangoFilterBackend

from apps.api.cash_flow.income.filters import IncomeFilter
from apps.api.cash_flow.income.serializer import IncomeSerializer, IncomeListSerializer
from apps.core.base_model_view_set import BaseModelViewSet
from apps.repositories.income_repo import IncomeRepo


class IncomeApiView(BaseModelViewSet):
    serializer_class = IncomeSerializer
    serializer_action_classes = {
        'list': IncomeListSerializer,
    }
    repository_class = IncomeRepo
    filter_backends = [DjangoFilterBackend]
    filterset_class = IncomeFilter