from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Income(models.Model):
    class Type(models.IntegerChoices):
        POSITIVE = 1, 'Positive'
        NEGATIVE = 2, 'Negative'

    MAP_TYPE_DICT = {
        1: 'Positive',
        2: 'Negative'
    }

    name = models.CharField(max_length=70)
    type = models.SmallIntegerField(choices=Type.choices, default=Type.NEGATIVE)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'income'


class Outcome(models.Model):
    class Type(models.IntegerChoices):
        FOR_SELF = 1, 'For Self'
        FOR_OTHER = 2, 'For Other'
        FOR_INVEST = 3, 'For Investment'

    MAP_TYPE_DICT = {
        1: 'For Myself',
        2: 'For Other',
        3: 'For Investing'
    }

    name = models.CharField(max_length=70)
    type = models.SmallIntegerField(choices=Type.choices, default=Type.FOR_INVEST)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'outcome'


class IncomeOutcomeStatistic(models.Model):
    cash = models.FloatField(default=0)  # Unit: A Thousand VND
    data_date_key = models.IntegerField()
    note = models.CharField(max_length=255, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        db_table = 'income_outcome_statistic'
