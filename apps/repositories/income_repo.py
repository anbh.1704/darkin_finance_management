from apps.models import Income
from apps.repositories.base_repo import BaseRepo


class IncomeRepo(BaseRepo):
    class Meta:
        model = Income