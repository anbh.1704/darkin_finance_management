from apps.models import IncomeOutcomeStatistic
from apps.repositories.base_polymorphic_repo import BasePolymorphicRepo
from apps.repositories.base_repo import BaseRepo


class IncomeOutcomeStatisticRepo(BaseRepo, BasePolymorphicRepo):
    class Meta:
        model = IncomeOutcomeStatistic
