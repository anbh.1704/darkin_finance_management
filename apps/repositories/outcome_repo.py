from apps.models import Outcome
from apps.repositories.base_repo import BaseRepo


class OutcomeRepo(BaseRepo):
    class Meta:
        model = Outcome
